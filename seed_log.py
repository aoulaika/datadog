from datetime import datetime
from random import choice
from time import sleep

USERS = ['jhon', 'james', 'elliot', 'elon']
RESOURCES = ['/users/details', '/pages/report', '/orders', '/orders/123']
METHODS = [
    'GET',
    'HEAD',
    'POST',
    'PUT',
    'DELETE',
    'CONNECT',
    'OPTIONS',
    'TRACE',
    'PATCH',
]
CODES = [200, 201, 202, 300, 301, 400, 401, 403, 409, 500]

while True:
    with open('access.log', 'a') as log:
        date = datetime.now().strftime('%d/%b/%Y:%H:%M:%S')
        log.write(
            f'127.0.0.1 - {choice(USERS)} [{date}] "{choice(METHODS)} {choice(RESOURCES)} HTTP/1.0" {choice(CODES)} 123\n'
        )
        sleep(0.09)
