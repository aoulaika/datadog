# how to use
## install requirements, either globaly or on a virtual environment
```
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```
## to run the app
```
usage: logstats.py [-h] [--filename FILENAME] [--threshold THRESHOLD]
                   [--debug]

HTTP log monitoring console program

optional arguments:
  -h, --help            show this help message and exit
  --filename FILENAME   log file path
  --threshold THRESHOLD
                        hits threshold (req/sec)
  --debug               enable debugging
```
## Usage log seeder
is used to seed the local access.log, to be used for testing
```
python seed_log.py
```

>The common logfile format is as follows:

    remotehost rfc931 authuser [date] "request" status bytes
```log
127.0.0.1 - james [12/Dec/2019:19:52:47] "HEAD /orders/123 HTTP/1.0" 301 123
127.0.0.1 - elon [12/Dec/2019:19:52:48] "OPTIONS /orders HTTP/1.0" 301 123
127.0.0.1 - elliot [12/Dec/2019:19:52:48] "GET /orders HTTP/1.0" 401 123
127.0.0.1 - james [12/Dec/2019:19:52:49] "DELETE /orders HTTP/1.0" 500 123
127.0.0.1 - elon [12/Dec/2019:19:52:49] "CONNECT /orders HTTP/1.0" 301 123
```
---
```
RepeatedTimer is used to print stats, check traffic hits treshold at specified intervals
```
---
```
LimitedSizeDict is user to store ordered default dict with a maxlen inspired by `collections.deqeue` to limit the stored information and prevent lot of memory usage
```