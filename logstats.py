import re
import threading
from argparse import ArgumentParser
from collections import Counter, defaultdict
from datetime import datetime as DT
from datetime import timedelta as TD

import sh
from tabulate import tabulate

from limited_size_dict import LimitedSizeDict
from repeated_timer import RepeatedTimer


class LogStats:
    def __init__(self, filename: str, threshold: str, debug: bool = False):
        self._filename = filename
        self._debug = debug
        self._threshold = threshold
        self._hight_traffic = False
        self._stats_counters = defaultdict(Counter)
        self._traffic_counter = LimitedSizeDict(maxlen=2, default_factory=Counter)
        self.re = re.compile(
            r'(?P<remotehost>(\d{1,3}\.){3}\d{1,3}) (?P<rfc931>.*) (?P<authuser>.*) \[(?P<date>.*)] "(?P<request>.*) (?P<resource>.*) (?P<httpversion>.*)" (?P<status>\d{3}) (?P<bytes>\d*)'
        )

    def run(self):
        RepeatedTimer(10, self._display_page_stats).start()
        RepeatedTimer(5, self._check_traffic).start()
        while True:
            for line in sh.tail("-f", self._filename, _iter=True):
                if self._debug:
                    print(line)
                matched = self.re.match(line)
                if matched:
                    self._update_stats_counters(matched.groupdict())
                    self._update_traffic_time(matched.groupdict()['date'])

    def _update_stats_counters(self, matched_dict: dict):
        '''
        update stats counters, 
        hits per resource, http method, calls per user, ...
        Parameters
        ----------
        matched_dict : dict
            resource url, /users/1/details for example
        '''
        self._update_page_hits(matched_dict['resource'])
        self._stats_counters['authusers'].update([matched_dict['authuser']])
        self._stats_counters['requests'].update([matched_dict['request']])
        self._stats_counters['status'].update([matched_dict['status']])

    def _update_page_hits(self, resource: str):
        '''
        update pages counters, check if resource is a page before updating
        Parameters
        ----------
        resource : str
            resource url, /users/1/details for example
        '''
        if resource.count('/') < 2:
            return

        self._stats_counters['pages'].update([resource[:resource.index('/', 1)]])

    def _update_traffic_time(self, date_str: str):
        '''
        Parameters
        ----------
        date_str : str
            log date, 11/Dec/2019:01:13:39 +0000 for example
        '''
        date = DT.strptime(date_str, '%d/%b/%Y:%H:%M:%S')
        key, value = date.strftime('%Y%m%d%H%M'), date.strftime('%S')
        self._traffic_counter[key].update([value])

    def _display_page_stats(self):
        '''
        display pages hits stats + other stats
        '''
        most_common = self._stats_counters['pages'].most_common()
        if most_common:
            most_viewed = most_common.pop(0)
            print(
                f"{DT.utcnow()} {most_viewed[0]} is the most viewed page in the last 10sec with {most_viewed[1]} hit(s).\n",
                "\nRest of pages ranking:\n", tabulate(most_common, headers=['page', 'hit(s)']),
                "\n\nUser requests ranking:\n",
                tabulate(self._stats_counters['authusers'].most_common(),
                         headers=['authuser', 'call(s)']), "\n\nMost http request method:\n",
                tabulate(self._stats_counters['requests'].most_common(),
                         headers=['request method', 'number']), "\n\nMost http status code:\n",
                tabulate(self._stats_counters['status'].most_common(), headers=['status',
                                                                                'number']), "\n")

        self._reset_stats_counters()

    def _check_traffic(self):
        now = DT.now()
        keys = [now.strftime('%Y%m%d%H%M'), (now - TD(minutes=1)).strftime('%Y%m%d%H%M')]
        if self._hight_traffic and not any(
            [keys[0] in self._traffic_counter, keys[1] in self._traffic_counter]):
            self._hight_traffic = False
            print(f'High traffic recovered, triggered at {now}')

        for key in keys:
            if key in self._traffic_counter and self._traffic_counter[key]:
                hits = self._traffic_counter[key].most_common(1)[0][1] or 0
                if not self._hight_traffic and hits > self._threshold:
                    self._hight_traffic = True
                    print(f'High traffic generated an alert - hits = {hits}, triggered at {now}')
                if self._hight_traffic and hits < self._threshold:
                    self._hight_traffic = False
                    print(f'High traffic recovered, triggered at {now}')

    def _reset_stats_counters(self):
        self._stats_counters['pages'].clear()
        self._stats_counters['authusers'].clear()
        self._stats_counters['requests'].clear()
        self._stats_counters['status'].clear()


if __name__ == "__main__":
    parser = ArgumentParser(description="HTTP log monitoring console program")
    parser.add_argument("--filename", type=str, default="/tmp/access.log", help="log file path")
    parser.add_argument("--threshold", type=int, default=10, help="hits threshold (req/sec)")
    parser.add_argument('--debug', action='store_true', default=False, help='enable debugging')
    args = parser.parse_args()
    l = LogStats(filename=args.filename, threshold=args.threshold, debug=args.debug)
    l.run()
